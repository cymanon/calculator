const Buttons = ({options, operation, special}) => {
    let buttons
    if (special) {
        buttons = options.map(opt => <button
            key={opt.name}
            onClick={opt.operation}>{opt.name}</button>)
    } else {
        buttons = options.map(opt => <button
            key={opt}
            onClick={() => operation(opt)}>{opt}</button>)
    }
    
    return (
        <div>
            {buttons}
        </div>
    )
}

export default Buttons