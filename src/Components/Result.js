import './Result.css'

const Result = ({result, preview, memory}) => {
    return (
        <div className='result-container'>
            <div className='sidenotes'>
                <p>ME {memory}</p>
                <p>{preview} =</p>
            </div >
            <p className='result'>{result}</p>
        </div>
    )
}

export default Result