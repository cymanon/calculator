import { Component } from 'react'
import './App.css'

import Result from './Components/Result'
import Buttons from './Components/Buttons'

class App extends Component {
  state = {
    firstValue: 0,
    secondValue: null,
    operationType: null,
    endValue: null,
    memory: null
  }

  numericValues = [7, 8, 9, 4, 5, 6, 1, 2, 3, 0]
  operationTypes = [':', 'x', '+', '-']

  clearDisplayedValue = () => {
    this.setState({
      firstValue: 0,
      secondValue: null,
      operationType: null,
      endValue: null,
    })
  }

  valuePicker = () => this.state.operationType ?
    this.state.secondValue :
    this.state.firstValue

  stateHandler = (value) => this.state.operationType ?
  this.setState({ secondValue: value }) :
  this.setState({ firstValue: value })

  numericButtonsHandler = value => {
    if (this.state.endValue) {
      this.clearDisplayedValue()
      this.setState({firstValue: value})
    } else if (typeof value === 'number') {

      const numericValue = this.valuePicker()
      if (value === 0 && numericValue === '0') return
      const valueToStore = numericValue ? numericValue + value.toString() : value.toString()
      this.stateHandler(valueToStore)
    }
  }

  operationTypesHandler = value => {
    if (this.state.endValue) {
      let newFirstValue = this.state.endValue
      this.clearDisplayedValue()
      this.setState({firstValue: newFirstValue})
    }

    this.setState({ operationType: value })
  }

  memoryHandler = () => {
    let memoriedValue
    if (this.state.endValue) {
      memoriedValue = this.state.endValue.toString()
      this.setState({ memory: memoriedValue })

      return
    }
    if (this.state.memory) {
      memoriedValue = this.state.memory.toString()
      this.stateHandler(memoriedValue)
    }
  }

  roundHandler = () => {
    const roundedValue = this.state.endValue && Math.round(this.state.endValue)
    
    this.setState({ endValue: roundedValue })
  }

  dotHandler = () => {
    let decimalValue = this.valuePicker()
    if (decimalValue.includes('.') || this.state.endValue) return
    decimalValue += '.'

    this.stateHandler(decimalValue)
  }

  changeSignHandler = () => {
    if (this.state.endValue) return
    let value = parseFloat(this.valuePicker())
    if (value !== 0) {
      this.stateHandler(value * -1)
    }

    return
  }

  memoryClearHandler = () => this.setState({memory: null})

  submitOperation = () => {
    if (this.state.endValue) return
    if (this.state.secondValue) {
      const first = parseFloat(this.state.firstValue)
      const second = parseFloat(this.state.secondValue)
      let value
      switch (this.state.operationType) {
        case ':':
          value = first / second
          break
        case '+':
          value = first + second
          break
        case 'x':
          value = first * second
          break
        case '-':
          value = first - second
          break
        default:
          break
      }

      this.setState({ endValue: value })
    } else return
  }



  render () {
    const preview = `${this.state.firstValue} ${this.state.operationType ? this.state.operationType : ''} ${this.state.secondValue ? this.state.secondValue : ''}`
    return (
      <div className='calculator'>
        <Result result={this.state.endValue} memory={this.state.memory} preview={preview}/>
        <div className='container'>
          <div className='flex-parent numbers'>
            <Buttons options={this.numericValues} operation={(value) => this.numericButtonsHandler(value)} />
            <button onClick={this.dotHandler}>.</button>
          </div>
          <div className='flex-parent'>
            <button onClick={this.clearDisplayedValue}>DEL</button>
            <button onClick={this.memoryHandler}>ME</button>
            <button onClick={this.memoryClearHandler}>MC</button>
            <button onClick={this.roundHandler}>RD</button>
            <Buttons options={this.operationTypes} operation={(value) => this.operationTypesHandler(value)} />
            <button onClick={this.changeSignHandler}>-/+</button>
            <button onClick={this.submitOperation} className='equality'>=</button>
          </div>
        </div>
      </div>
    )
  }
}

export default App